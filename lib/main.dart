import 'package:flutter/material.dart';

import 'package:codigo16200135/routes/routes.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App de pagos',
      debugShowCheckedModeBanner: false,
      initialRoute: 'login',
      routes: routes(),
    );
  }
}
