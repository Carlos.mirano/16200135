// To parse this JSON data, do
//
//     final usuario = usuarioFromJson(jsonString);

import 'dart:convert';

Usuario usuarioFromJson(String str) => Usuario.fromJson(json.decode(str));

String usuarioToJson(Usuario data) => json.encode(data.toJson());

class Usuario {
  Usuario({
    this.idUsuario,
    this.userName,
    this.pass,
    this.codAlumno,
    this.apePaterno,
    this.nomAlumno,
    this.dniM,
    this.mail,
  });

  int idUsuario;
  String userName;
  String pass;
  String codAlumno;
  String apePaterno;
  String nomAlumno;
  String dniM;
  dynamic mail;

  factory Usuario.fromJson(Map<String, dynamic> json) => Usuario(
        idUsuario: json["idUsuario"],
        userName: json["userName"],
        pass: json["pass"],
        codAlumno: json["codAlumno"],
        apePaterno: json["apePaterno"],
        nomAlumno: json["nomAlumno"],
        dniM: json["dniM"],
        mail: json["mail"],
      );

  Map<String, dynamic> toJson() => {
        "idUsuario": idUsuario,
        "userName": userName,
        "pass": pass,
        "codAlumno": codAlumno,
        "apePaterno": apePaterno,
        "nomAlumno": nomAlumno,
        "dniM": dniM,
        "mail": mail,
      };
}
