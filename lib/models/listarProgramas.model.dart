// To parse this JSON data, do
//
//     final listarProgramas = listarProgramasFromJson(jsonString);

import 'dart:convert';

List<ListarProgramas> listarProgramasFromJson(String str) =>
    List<ListarProgramas>.from(
        json.decode(str).map((x) => ListarProgramas.fromJson(x)));

String listarProgramasToJson(List<ListarProgramas> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ListarProgramas {
  ListarProgramas({
    this.codAlumno,
    this.apePaterno,
    this.apeMaterno,
    this.nomAlumno,
    this.codEspecialidad,
    this.codTipIngreso,
    this.codSitu,
    this.codPerm,
    this.anioIngreso,
    this.dniM,
    this.idPrograma,
    this.nomPrograma,
    this.siglaProg,
  });

  String codAlumno;
  String apePaterno;
  String apeMaterno;
  String nomAlumno;
  String codEspecialidad;
  String codTipIngreso;
  String codSitu;
  String codPerm;
  String anioIngreso;
  String dniM;
  int idPrograma;
  String nomPrograma;
  String siglaProg;

  factory ListarProgramas.fromJson(Map<String, dynamic> json) =>
      ListarProgramas(
        codAlumno: json["codAlumno"],
        apePaterno: json["apePaterno"],
        apeMaterno: json["apeMaterno"],
        nomAlumno: json["nomAlumno"],
        codEspecialidad: json["codEspecialidad"],
        codTipIngreso: json["codTipIngreso"],
        codSitu: json["codSitu"],
        codPerm: json["codPerm"],
        anioIngreso: json["anioIngreso"],
        dniM: json["dniM"],
        idPrograma: json["idPrograma"],
        nomPrograma: json["nom_programa"],
        siglaProg: json["siglaProg"],
      );

  Map<String, dynamic> toJson() => {
        "codAlumno": codAlumno,
        "apePaterno": apePaterno,
        "apeMaterno": apeMaterno,
        "nomAlumno": nomAlumno,
        "codEspecialidad": codEspecialidad,
        "codTipIngreso": codTipIngreso,
        "codSitu": codSitu,
        "codPerm": codPerm,
        "anioIngreso": anioIngreso,
        "dniM": dniM,
        "idPrograma": idPrograma,
        "nom_programa": nomPrograma,
        "siglaProg": siglaProg,
      };
}
