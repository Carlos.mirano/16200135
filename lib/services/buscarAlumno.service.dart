import 'package:http/http.dart' as http;

import 'package:codigo16200135/models/alumno.model.dart';

Future<Usuario> buscarAlumno(String dni, String clave) async {
  String url =
      "https://sigapdev2-consultarecibos-back.herokuapp.com/usuario/alumnoprograma/buscar/$dni/$clave";

  final resp = await http.get(url);

  if (resp.statusCode == 200) {
    return usuarioFromJson(resp.body);
  }

  return null;
}
