import 'package:http/http.dart' as http;

import 'package:codigo16200135/models/listarProgramas.model.dart';

Future<List<ListarProgramas>> listarProgramas(String dni) async {
  String url =
      "https://sigapdev2-consultarecibos-back.herokuapp.com/alumnoprograma/buscard/$dni";

  final resp = await http.get(url);

  if (resp.statusCode == 200) {
    return listarProgramasFromJson(resp.body);
  }

  return null;
}
