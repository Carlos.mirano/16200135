import 'package:http/http.dart' as http;

Future<dynamic> buscarAlumno(String dni, String clave) async {
  String url =
      "https://sigapdev2-consultarecibos-back.herokuapp.com/recaudaciones/alumno/concepto/listar_cod/17207064";

  final resp = await http.get(url);

  if (resp.statusCode == 200) {
    return resp.body;
  }

  return null;
}
