import 'package:codigo16200135/services/buscarAlumno.service.dart';
import 'package:codigo16200135/shared/alumno.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Column(
            children: <Widget>[
              formLogin(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget formLogin(BuildContext context) {
    TextEditingController dni = new TextEditingController();
    TextEditingController clave = new TextEditingController();
    final _formKey = GlobalKey<FormState>();
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 10.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              controller: dni,
              decoration: InputDecoration(
                hintText: 'DNI',
              ),
            ),
            SizedBox(
              height: 20.0,
            ),
            TextFormField(
              controller: clave,
              decoration: InputDecoration(
                hintText: 'Contraseña',
              ),
              obscureText: true,
            ),
            SizedBox(
              height: 20.0,
            ),
            SizedBox(
              width: double.infinity,
              height: 50.0,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0)),
                child: Text(
                  'Iniciar sesion',
                  style: TextStyle(fontSize: 20.0),
                ),
                textColor: Colors.white,
                color: Colors.blue,
                onPressed: () {
                  final form = _formKey.currentState;
                  if (form.validate()) {
                    form.save();
                    verificarUsuario(context, dni, clave);
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void verificarUsuario(BuildContext context, TextEditingController dni,
      TextEditingController clave) async {
    var resp = await buscarAlumno(dni.text, clave.text);
    if (resp == null) {
      print("Error al iniciar sesión");
    } else {
      Navigator.pushNamed(context, 'listarProgramas',
          arguments: Alumno(alumno: resp));
    }
  }
}
