import 'package:codigo16200135/models/alumno.model.dart';
import 'package:codigo16200135/services/listarProgramas.service.dart';
import 'package:codigo16200135/shared/alumno.dart';
import 'package:codigo16200135/shared/listProgramas.dart';
import 'package:flutter/material.dart';

class ListarProgramas extends StatefulWidget {
  ListarProgramas({Key key}) : super(key: key);

  @override
  _ListarProgramasState createState() => _ListarProgramasState();
}

class _ListarProgramasState extends State<ListarProgramas> {
  @override
  Widget build(BuildContext context) {
    final Alumno alumno = ModalRoute.of(context).settings.arguments;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Lista de programas"),
        ),
        body: Container(
          child: _listarProgramas(context, alumno.alumno),
        ),
      ),
    );
  }

  Widget _listarProgramas(BuildContext context, Usuario usuario) {
    return Container(
      child: FutureBuilder(
        future: listarProgramas(usuario.dniM),
        initialData: [],
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            return ListView(
              padding: EdgeInsets.all(10.0),
              children: _programas(context, snapshot.data),
            );
          }
        },
      ),
    );
  }

  List<Widget> _programas(BuildContext context, dynamic listProgramas) {
    final List<Widget> options = [];

    listProgramas.forEach((op) {
      final widgetTemp = Container(
        child: Card(
          elevation: 5.0,
          shadowColor: Colors.blue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          child: InkWell(
            onTap: () {
              Navigator.pushNamed(context, 'estadoPagos',
                  arguments: ListProgramas(list: op));
            },
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.card_travel,
                    color: Colors.blue,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Expanded(
                    child: Text(
                      "${op.nomPrograma}",
                      style: TextStyle(fontSize: 18.0),
                    ),
                  ),
                  Icon(
                    Icons.keyboard_arrow_right,
                    color: Colors.blue,
                    size: 30.0,
                  )
                ],
              ),
            ),
          ),
        ),
      );
      options.add(widgetTemp);
    });

    return options;
  }
}
