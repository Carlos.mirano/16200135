import 'package:codigo16200135/pages/Login.dart';
import 'package:codigo16200135/pages/estadoPagos.dart';
import 'package:codigo16200135/pages/listarProgramas.dart';
import 'package:flutter/material.dart';

Map<String, WidgetBuilder> routes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => Login(),
    'listarProgramas': (BuildContext context) => ListarProgramas(),
    'estadoPagos': (BuildContext context) => EstadoPagos(),
  };
}
